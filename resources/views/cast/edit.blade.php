@extends('layout.master')

@section('title')
    List Cast
@endsection

@section('content')
    
<div>
    <h2>Edit Cast {{$cast->id}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT') 
        {{-- //method put berfungsi sebagai edit --}}
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama">
            {{-- @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror --}}
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" id="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio" id="bio" value="{{$cast->bio}}" placeholder="Masukkan Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection